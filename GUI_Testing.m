function varargout = GUI_Testing(varargin)
% GUI_Testing MATLAB code for GUI_Testing.fig
%      GUI_Testing, by itself, creates a new GUI_Testing or raises the existing
%      singleton*.
%
%      H = GUI_Testing returns the handle to a new GUI_Testing or the handle to
%      the existing singleton*.
%
%      GUI_Testing('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_Testing.M with the given input arguments.
%
%      GUI_Testing('Property','Value',...) creates a new GUI_Testing or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI_Testing before GUI_Testing_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_Testing_OpeningFcn via varargin.
%
%      *See GUI_Testing Options on GUIDE's Tools menu.  Choose "GUI_Testing allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_Testing

% Last Modified by GUIDE v2.5 16-May-2016 09:24:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_Testing_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_Testing_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% ------------------------------------------------------------------------------
% --- Executes just before GUI_Testing is made visible.
% ------------------------------------------------------------------------------
function GUI_Testing_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to GUI_Testing (see VARARGIN)

    % Choose default command line output for GUI_Testing
    handles.output = hObject;
    handles.video_path = '';

    load('features.mat');
    handles.features = features;

%     handles.cbdown = 100;
%     handles.cbup = 125;
%     handles.crdown = 140;
%     handles.crup = 173;
    
    handles.cbdown = 73;
    handles.cbup = 120;
    handles.crdown = 137;
    handles.crup = 167;
    
%     handles.min_blob = 50;
%     handles.max_blob = 200;
    
    handles.min_blob = 100;
    handles.max_blob = 500;

    % Update handles structure
    guidata(hObject, handles);

    % UIWAIT makes GUI_Testing wait for user response (see UIRESUME)
    % uiwait(handles.figure1);


% ------------------------------------------------------------------------------
% --- Outputs from this function are returned to the command line.
% ------------------------------------------------------------------------------
function varargout = GUI_Testing_OutputFcn(hObject, eventdata, handles)
    % Get default command line output from handles structure
    varargout{1} = handles.output;


% ------------------------------------------------------------------------------
% --- LOAD BUTTON
% ------------------------------------------------------------------------------
function button_load_Callback(hObject, eventdata, handles)
    [filename, pathname] = uigetfile({'*.avi; *.mp4'}, 'Select a video.');
    %if uigetfile({'*.avi; *.mp4'}, 'Select a video.') == 0
    if isequal(filename, 0)
        set(handles.text_status, 'String', 'Loading video has been canceled.');
    else
        handles.video_path = strcat(pathname, filename);
        handles.video_src = vision.VideoFileReader( ...
            handles.video_path, ...
            'VideoOutputDataType', 'uint8');

        handles.video_src.PlayCount = 1;

        % reset the running video when loading a new video
        reset(handles.video_src);
        set(handles.button_play,'string', 'Play');

        set(handles.text_status, 'String', handles.video_path);
        guidata(hObject,handles);
    end

% ------------------------------------------------------------------------------
% --- PLAY BUTTON
% ------------------------------------------------------------------------------
function button_play_Callback(hObject, eventdata, handles)
    if strcmp(handles.video_path, '')
        set(handles.text_status, 'String', 'WARNING! No video has been loaded.');
    else
        is_text_play = strcmp(get(hObject,'string'), 'Play');
        is_text_continue  = strcmp(get(hObject,'string'), 'Continue');

        if is_text_play
           % Two cases: (1) starting first time, or (2) restarting
           % Start from first frame
           if isDone(handles.video_src)
              reset(handles.video_src);
           end
        end

        if (is_text_play || is_text_continue)
            set(hObject,'string','Pause');
        else
            set(hObject,'string','Continue');
        end

        % initialization
        frame = zeros(1000, 1000);
        skin = zeros(1000, 1000);
        hand = zeros(1000, 1000);
        img = zeros(256, 256);
        gfd = zeros(52, 1);
        output_text = '';
        distance = 0;
        features = handles.features;
        [m n] = size(features);
        training_image_number = n;

        while strcmp(get(hObject,'string'),'Pause') && ~isDone(handles.video_src)
            tic
            axes(handles.ax_input_video);
            frame = step(handles.video_src);
            img = imresize(frame, [128 128]);

            skin = logical(skin_segmentation( ...
                img, ...
                handles.cbdown, ...
                handles.cbup, ...
                handles.crdown, ...
                handles.crup ...
            ));

            hand = hand_segmentation( ...
                skin, ...
                handles.min_blob, ...
                handles.max_blob ...
            );

            if hand == 0
                gfd = 0;
            else
                gfd = generic_fourier_descriptors(hand, 3, 12);

                % classification
                format bank
                for n = 1 : training_image_number
                    distance = euclidean_distance(gfd, features(n).feature);
                    % distance
                    if (distance < 0.01)
                       output_text = features(n).alphabet;
                    end
                end
            end

            set(handles.text_result, 'String', output_text);

            if strcmp(get(hObject,'string'), 'Pause')
                imshow(frame);
            end

            axes(handles.ax_hand_segmentation);

            if strcmp(get(hObject,'string'), 'Pause')
                imshow(hand);
            end
            %axes(handles.ax_hand_segmentation);
            %imshow(img);

            %axes(handles.ax_cb);
            %imshow(img);

            %axes(handles.ax_cr);
            %imshow(img);
            toc
        end

        if isDone(handles.video_src)
           set(hObject,'string', 'Play');
        end
    end

% ------------------------------------------------------------------------------
% --- STOP BUTTON
% ------------------------------------------------------------------------------
function button_stop_Callback(hObject, eventdata, handles)
    if strcmp(handles.video_path, '')
        set(handles.text_status, 'String', 'WARNING! No video has been loaded.');
    else
        reset(handles.video_src);
        set(handles.button_play,'string', 'Play');
    end

% ------------------------------------------------------------------------------
% --- Executes during object deletion, before destroying properties.
% ------------------------------------------------------------------------------
function figure1_DeleteFcn(hObject, eventdata, handles)

% ------------------------------------------------------------------------------
% --- Executes when user attempts to close figure1.
% ------------------------------------------------------------------------------
function figure1_CloseRequestFcn(hObject, eventdata, handles)
    % Hint: delete(hObject) closes the figure
    delete(hObject);
