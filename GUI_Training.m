function varargout = GUI_Training(varargin)
% GUI_Training MATLAB code for GUI_Training.fig
%      GUI_Training, by itself, creates a new GUI_Training or raises the existing
%      singleton*.
%
%      H = GUI_Training returns the handle to a new GUI_Training or the handle to
%      the existing singleton*.
%
%      GUI_Training('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_Training.M with the given input arguments.
%
%      GUI_Training('Property','Value',...) creates a new GUI_Training or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI_Training before GUI_Training_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_Training_OpeningFcn via varargin.
%
%      *See GUI_Training Options on GUIDE's Tools menu.  Choose "GUI_Training allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_Training

% Last Modified by GUIDE v2.5 20-May-2016 14:30:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_Training_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_Training_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% ------------------------------------------------------------------------------
% --- Executes just before GUI_Training is made visible.
% ------------------------------------------------------------------------------
function GUI_Training_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_Training (see VARARGIN)

    % Choose default command line output for GUI_Training
    handles.output = hObject;

    handles.video_path = '';
    
    
    % set slider value
    set(handles.slider_cb_down, 'Value', 73);
    set(handles.slider_cb_up, 'Value', 120);
    set(handles.slider_cr_down, 'Value', 137);
    set(handles.slider_cr_up, 'Value', 167);

    % set slider value text
    set(handles.cbdown_valtxt, 'String', ...
            int2str(get(handles.slider_cb_down, 'Value')));
    set(handles.cbup_valtxt, 'String', ...
            int2str(get(handles.slider_cb_up, 'Value')));
    set(handles.crdown_valtxt, 'String', ...
            int2str(get(handles.slider_cr_down, 'Value')));
    set(handles.crup_valtxt, 'String', ...
            int2str(get(handles.slider_cr_up, 'Value')));
        
    % set blob size form value
    set(handles.min_blob_form, 'String', int2str(150));
    set(handles.max_blob_form, 'String', int2str(0));
    set(handles.max_blob_form, 'Enable', 'off');

    % initialize global variable
    load('features.mat');
    handles.features = features;

    handles.current_frame = 330;
    handles.image_frame = zeros(1000, 1000);
    handles.binary_image = zeros(1000, 1000);
    handles.min_blob = 100;
    handles.max_blob = 200;
    handles.alphabet = '';

    handles.cbdown = get(handles.slider_cb_down, 'Value');
    handles.cbup = get(handles.slider_cb_up, 'Value');
    handles.crdown = get(handles.slider_cr_down, 'Value');
    handles.crup = get(handles.slider_cr_up, 'Value');

    % Update handles structure
    guidata(hObject, handles);

% UIWAIT makes GUI_Training wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% ------------------------------------------------------------------------------
% --- Outputs from this function are returned to the command line.
% ------------------------------------------------------------------------------
function varargout = GUI_Training_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% ------------------------------------------------------------------------------
% --- PREVIOUS FRAME BUTTON - Executes on button press in prev_frame_button.
% ------------------------------------------------------------------------------
function prev_frame_button_Callback(hObject, eventdata, handles)
    if strcmp(handles.video_path, '')
        set(handles.text_status, 'String', 'WARNING! No video has been loaded.');
    elseif handles.current_frame <= 1
        set(handles.text_status, 'String', 'WARNING! Cannot read nonzero frame number.');
    else
        tic
        axes(handles.ax_video_preview);
        handles.current_frame = handles.current_frame - 1;
        frame = read(handles.video_src, handles.current_frame);
        imshow(frame);
        img = imresize(frame, [128 128]);

        skin = logical(skin_segmentation( ...
            img, ...
            handles.cbdown, ...
            handles.cbup, ...
            handles.crdown, ...
            handles.crup ...
        ));

        hand = hand_segmentation( ...
            skin, ...
            handles.min_blob, ...
            handles.max_blob ...
        );

        axes(handles.ax_hand_segmentation);
        imshow(hand);

        handles.image_frame = frame;

        % set frame number text
        set( ...
            handles.text_frame_number, ...
            'String', ...
            strcat({'Frame: '}, int2str(handles.current_frame)) ...
        );

        set(handles.text_status, 'String', '');
        %axes(handles.ax_cb);
        %imshow(img);

        %axes(handles.ax_cr);
        %imshow(img);
        % Update handles structure
        guidata(hObject, handles);
        toc
    end

% ------------------------------------------------------------------------------
% --- NEXT FRAME BUTTON - Executes on button press in next_frame_button.
% ------------------------------------------------------------------------------
function next_frame_button_Callback(hObject, eventdata, handles)
    if strcmp(handles.video_path, '')
        set(handles.text_status, 'String', 'WARNING! No video has been loaded.');
    elseif handles.current_frame >= handles.video_length
        set(handles.text_status, 'String', 'WARNING! End of video frame.');
    else
        tic
        axes(handles.ax_video_preview);
        handles.current_frame = handles.current_frame + 1;
        frame = read(handles.video_src, handles.current_frame);
        imshow(frame);
        img = imresize(frame, [128 128]);

        skin = logical(skin_segmentation( ...
            img, ...
            handles.cbdown, ...
            handles.cbup, ...
            handles.crdown, ...
            handles.crup ...
        ));

        hand = hand_segmentation( ...
            skin, ...
            handles.min_blob, ...
            handles.max_blob ...
        );

        axes(handles.ax_hand_segmentation);
        imshow(hand);

        handles.image_frame = frame;

        % set frame number text
        set( ...
            handles.text_frame_number, ...
            'String', ...
            strcat({'Frame: '}, int2str(handles.current_frame)) ...
        );

        set(handles.text_status, 'String', '');

        %axes(handles.ax_cb);
        %imshow(img);

        %axes(handles.ax_cr);
        %imshow(img);
        % Update handles structure
        guidata(hObject, handles);
        toc
    end


% ------------------------------------------------------------------------------
% --- ALPHABET TEXT FIELD
% ------------------------------------------------------------------------------
function alphabet_form_Callback(hObject, eventdata, handles)
% ------------------------------------------------------------------------------
% hObject    handle to alphabet_form (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alphabet_form as text
%        str2double(get(hObject,'String')) returns contents of alphabet_form as a double
    handles.alphabet = '';


% ------------------------------------------------------------------------------
% --- ALPHABET TEXT FIELD INIT
% --- Executes during object creation, after setting all properties.
% ------------------------------------------------------------------------------
function alphabet_form_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alphabet_form (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), ...
        get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

% ------------------------------------------------------------------------------
% --- LOAD VIDEO BUTTON - Executes on button press in load_button.
% ------------------------------------------------------------------------------
function load_button_Callback(hObject, eventdata, handles)
    [filename, pathname] = uigetfile({'*.avi; *.mp4'}, 'Select a video.');

    if isequal(filename, 0)
        set(handles.text_status, 'String', 'Loading video has been canceled.');
    else

        handles.video_path = strcat(pathname, filename);
        handles.video_src = VideoReader(handles.video_path);
        handles.video_length = handles.video_src.NumberOfFrames;

        set( ...
            handles.text_frame_number, ...
            'String', ...
            strcat({'Frame: '}, int2str(handles.current_frame)) ...
        );

        set(handles.text_status, 'String', ...
            strcat({'DONE! '}, handles.video_path, {' and '}, ...
                    {' features.mat has been loaded.'}) ...
        );

        guidata(hObject,handles);
    end

% ------------------------------------------------------------------------------
% --- SAVE FEATURE BUTTON - Executes on button press in save_button.
% ------------------------------------------------------------------------------
function save_button_Callback(hObject, eventdata, handles)
% hObject    handle to save_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    img = imresize(handles.image_frame, [128 128]);

    skin = logical(skin_segmentation( ...
        img, ...
        handles.cbdown, ...
        handles.cbup, ...
        handles.crdown, ...
        handles.crup ...
    ));

    hand = hand_segmentation( ...
        skin, ...
        handles.min_blob, ...
        handles.max_blob ...
    );

    gfd = generic_fourier_descriptors(hand, 3, 12);

    features = handles.features;

    [m n] = size(features);
    features(n+1) = struct( ...
        'alphabet', ...
        get(handles.alphabet_form, 'String'), ...
        'feature', ...
        gfd ...
    );

    handles.features = features;
    guidata(hObject, handles);

    save('features.mat', 'features');

% ------------------------------------------------------------------------------
% --- SLIDER CB DOWN - Executes on slider movement.
% ------------------------------------------------------------------------------
function slider_cb_down_Callback(hObject, eventdata, handles)
% hObject    handle to slider_cb_down (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

    set(handles.cbdown_valtxt, 'String', ...
            int2str(get(hObject, 'Value')));

    handles.cbdown = get(hObject, 'Value');
    guidata(hObject, handles);

    skin = logical(skin_segmentation( ...
        handles.image_frame, ...
        handles.cbdown, ...
        handles.cbup, ...
        handles.crdown, ...
        handles.crup ...
    ));

    hand = hand_segmentation( ...
        skin, ...
        handles.min_blob, ...
        handles.max_blob ...
    );

    axes(handles.ax_hand_segmentation);
    imshow(hand);


% ------------------------------------------------------------------------------
% --- SLIDER CB DOWN INIT
% --- Executes during object creation, after setting all properties.
% ------------------------------------------------------------------------------
function slider_cb_down_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_cb_down (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

    %set(handles.cbdown_valtxt, 'String', int2str(get(hObject, 'Value')));
    %cbdown = get(hObject, 'Value')


% ------------------------------------------------------------------------------
% --- SLIDER CB UP -Executes on slider movement.
% ------------------------------------------------------------------------------
function slider_cb_up_Callback(hObject, eventdata, handles)
% hObject    handle to slider_cb_up (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

    set(handles.cbup_valtxt, 'String', ...
            int2str(get(hObject, 'Value')));

    handles.cbup = get(hObject, 'Value');
    guidata(hObject, handles);

    skin = logical(skin_segmentation( ...
        handles.image_frame, ...
        handles.cbdown, ...
        handles.cbup, ...
        handles.crdown, ...
        handles.crup ...
    ));

    hand = hand_segmentation( ...
        skin, ...
        handles.min_blob, ...
        handles.max_blob ...
    );

    axes(handles.ax_hand_segmentation);
    imshow(hand);


% ------------------------------------------------------------------------------
% --- SLIDER CB UP INIT
% --- Executes during object creation, after setting all properties.
% ------------------------------------------------------------------------------
function slider_cb_up_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_cb_up (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% ------------------------------------------------------------------------------
% --- SLIDER CR DOWN - Executes on slider movement.
% ------------------------------------------------------------------------------
function slider_cr_down_Callback(hObject, eventdata, handles)
% hObject    handle to slider_cr_down (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

    set(handles.crdown_valtxt, 'String', ...
            int2str(get(hObject, 'Value')));

    handles.crdown = get(hObject, 'Value');
    guidata(hObject, handles);

    skin = logical(skin_segmentation( ...
        handles.image_frame, ...
        handles.cbdown, ...
        handles.cbup, ...
        handles.crdown, ...
        handles.crup ...
    ));

    hand = hand_segmentation( ...
        skin, ...
        handles.min_blob, ...
        handles.max_blob ...
    );

    axes(handles.ax_hand_segmentation);
    imshow(hand);


% ------------------------------------------------------------------------------
% --- SLIDER CR DOWN INIT
% --- Executes during object creation, after setting all properties.
% ------------------------------------------------------------------------------
function slider_cr_down_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_cr_down (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% ------------------------------------------------------------------------------
% --- SLIDER CR UP - Executes on slider movement.
% ------------------------------------------------------------------------------
function slider_cr_up_Callback(hObject, eventdata, handles)
% hObject    handle to slider_cr_up (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

    set(handles.crup_valtxt, 'String', ...
            int2str(get(hObject, 'Value')));

    handles.crup = get(hObject, 'Value');
    guidata(hObject, handles);

    skin = logical(skin_segmentation( ...
        handles.image_frame, ...
        handles.cbdown, ...
        handles.cbup, ...
        handles.crdown, ...
        handles.crup ...
    ));

    hand = hand_segmentation( ...
        skin, ...
        handles.min_blob, ...
        handles.max_blob ...
    );

    axes(handles.ax_hand_segmentation);
    imshow(hand);

% ------------------------------------------------------------------------------
% --- SLIDER CR UP INIT
% --- Executes during object creation, after setting all properties.
% ------------------------------------------------------------------------------
function slider_cr_up_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_cr_up (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% ------------------------------------------------------------------------------
% --- MIN BLOB SIZE TEXT FIELD
% ------------------------------------------------------------------------------
function min_blob_form_Callback(hObject, eventdata, handles)
% hObject    handle to min_blob_form (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of min_blob_form as text
%        str2double(get(hObject,'String')) returns contents of min_blob_form as a double
    handles.min_blob = str2num(get(hObject, 'String'));
    guidata(hObject, handles);

    skin = logical(skin_segmentation( ...
        handles.image_frame, ...
        handles.cbdown, ...
        handles.cbup, ...
        handles.crdown, ...
        handles.crup ...
    ));

    hand = hand_segmentation( ...
        skin, ...
        handles.min_blob, ...
        handles.max_blob ...
    );

    axes(handles.ax_hand_segmentation);
    imshow(hand);


% ------------------------------------------------------------------------------
% --- MIN BLOB SIZE TEXT FIELD INIT
% --- Executes during object creation, after setting all properties.
% ------------------------------------------------------------------------------
function min_blob_form_CreateFcn(hObject, eventdata, handles)
% hObject    handle to min_blob_form (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% ------------------------------------------------------------------------------
% --- MAX BLOB SIZE TEXT FIELD
% ------------------------------------------------------------------------------
function max_blob_form_Callback(hObject, eventdata, handles)
% hObject    handle to max_blob_form (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of max_blob_form as text
%        str2double(get(hObject,'String')) returns contents of max_blob_form as a double
    handles.max_blob = str2num(get(hObject, 'String'));
    handles.max_blob
    guidata(hObject, handles);

    skin = logical(skin_segmentation( ...
        handles.image_frame, ...
        handles.cbdown, ...
        handles.cbup, ...
        handles.crdown, ...
        handles.crup ...
    ));

    hand = hand_segmentation( ...
        skin, ...
        handles.min_blob, ...
        handles.max_blob ...
    );

    axes(handles.ax_hand_segmentation);
    imshow(hand);


% ------------------------------------------------------------------------------
% --- MAX BLOB SIZE TEXT FIELD INIT
% --- Executes during object creation, after setting all properties.
% ------------------------------------------------------------------------------
function max_blob_form_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max_blob_form (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
