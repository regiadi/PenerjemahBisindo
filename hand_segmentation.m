function hand = hand_segmentation(skin, minblob, maxblob)
    % skin = xor(bwareaopen(img, minblob),  bwareaopen(img, maxblob));
    
    skin = bwareaopen(skin, minblob);

%     cc = bwconncomp(skin);
%     L = labelmatrix(cc);
%     s = regionprops(L, 'Area');
%     area = [s.Area]
    % figure(), imshow(bwareaopen(skin, 500));

    cc = bwconncomp(skin);
    L = labelmatrix(cc);
    s = regionprops(L, 'Centroid', 'PixelIdxList');

    [m n] = size(s);
    for j = 1 : m
        if s(j).Centroid(2) < 55
            skin(s(j).PixelIdxList) = 0;
        end
    end
    
    s  = regionprops(skin, 'centroid', 'Extrema');
    centroids = cat(1, s.Centroid);
    [cM cN] = size(centroids);

    if cM == 2
        x1 = centroids(1, 1);
        y1 = centroids(1, 2);
        x2 = centroids(2, 1);
        y2 = centroids(2, 2);

        % connect two separated blobs with bresenham line
        [x, y]=bresenham_line(x1,y1, x2,y2);

        for a = 1 : length(x)
            skin(y(a), x(a)) = 1;
        end

        % centering the centroid of the connected blobs
        hand = logical(center_object(logical(skin)));
    elseif cM == 1
        % if object is just one, return centered blob
        hand = logical(center_object(logical(skin)));
    else
        % if object is less than 1 and more than 2, return blank binary image
        hand =0;
    end
end
