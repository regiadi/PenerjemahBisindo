function ycbcrImage = rgb_to_ycbcr(rgbImage)
    %[m, n, c] = size(rgbImage);
    %ycbcr = uint8(zeros(m, n, c));

    R = double(rgbImage(:, :, 1));
    G = double(rgbImage(:, :, 2));
    B = double(rgbImage(:, :, 3));

    Y = 0.2990 * R + 0.5870 * G + 0.1140 * B + 0;
    Cb = -0.1687 * R + -0.3313 *G + 0.5000 * B + 128;
    Cr = 0.5000 * R + -0.4187 * G + -0.0813 * B + 128;

    ycbcrImage(:, :, 1) = uint8(Y);
    ycbcrImage(:, :, 2) = uint8(Cb);
    ycbcrImage(:, :, 3) = uint8(Cr);
end
