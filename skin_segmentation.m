function skin = skin_segmentation(img, cbdown, cbup, crdown, crup)
    %img = imresize(img, [256 256]);
    % img = imadjust(img,[],[], 0.8);

    [img_height, img_width, dim] = size(img);
    skin = zeros(img_height, img_width);

    % mengubah RGB menjadi YCbCr
    img_ycbcr = rgb_to_ycbcr(img);
    Cb = img_ycbcr(:,:,2);
    Cr = img_ycbcr(:,:,3);

    %figure(3), imshow(Cb);
    %title('Cb');

    %figure(4), imshow(Cr);
    %title('Cr');

    % mendeteksi skin region
    [r, c, v] = find(Cb >= cbdown & Cb <= cbup & Cr >= crdown & Cr <= crup);
    numind = size(r, 1);

    % menandai skin region
    for x = 1 : numind
        skin(r(x), c(x)) = 1;
    end
end
